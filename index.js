// SERVER MODULS INIT
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
require("dotenv").config();
const url = process.env.URL_API;
const PORT = process.env.PORT || 5002;
const fetch = require('node-fetch');

// ===== SOCKET PART =====

io.on('connection', function(socket){

  // This socket send a notification to a merchant when a customer make an order using sendNotificationToMerchant method
  socket.on('notifyMerchant', function(data){
      sendNotificationToMerchant(data)
  })

  // This socket send a notification to an user when merchant accept to prepare an order using sendNotificationToClientForOrder method
  socket.on('notifyClientForOrder', function(data){
    sendNotificationToClientForOrder(data)
  })

  // This socket send a notification to a merchant and a customer when a deliverer handle an order using sendNotificationToClientAndMerchant method
  socket.on('notifyClientAndMerchant', function(data){
      sendNotificationToClientAndMerchant(data)
  })

  // This socket send a notification to a client when a command is in delivery using sendNotificationToClientForDelivery method
  socket.on('notifyClientForDelivery', function(data){
    sendNotificationToClientForDelivery(data)
  })

  //This socket send a notification to User or Deliverer when there s a new message
  socket.on('notifyNewMessage', function(data){
    if(data[1]!=data[0].user.idUser){
      sendNotificationToClientForNewMessage(data[0])
    }
    else{
      sendNotificationToDelivererForNewMessage(data[0])
    }
  })

}); // End of IO

// ===== FUNCTIONS TO MAKE PUSH NOTIFICATIONS USING EXPO NOTIFICATION SERVICE =====

// Notif to merchant
async function sendNotificationToMerchant(data) {

  const message = {
    to: data.merchant.expoToken,
    sound: 'default',
    title: `Commande n°${data._id}`,
    body: `${data.user.firstName} vous a passé une nouvelle commande`,
  };
    
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      'host': 'exp.host',
      'Accept': 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });

} // End of function

// Notify client when order is accepted by merchant
async function sendNotificationToClientForOrder(data) {

  const message = {
    to: data.user.expoToken,
    sound: 'default',
    title: `Commande n°${data._id}`,
    body: `${data.merchant.enterprise} a accepté votre commande`,
  };
    
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      'host': 'exp.host',
      'Accept': 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });

} // End of function

// Notif to client & merchant
async function sendNotificationToClientAndMerchant(data) {

  const messageToMerchant = {
    to: data.merchant.expoToken,
    sound: 'default',
    title: `Prise en charge commande n°${data._id}`,
    body: `${data.deliverer.firstName} va venir récupérer la commande n°${data._id} ! \n Veuillez la préparer s'il vous plait`,
  };

  const messageToUser = {
    to: data.user.expoToken,
    sound: 'default',
    title: `Prise en charge commande n°${data._id}`,
    body: `${data.deliverer.firstName} va s'occuper de vous livrer la commande n°${data._id} ! \n Vous pouvez désormais discuter ensemble dans votre messagerie personnelle`,
  };
  
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      'host': 'exp.host',
      'Accept': 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(messageToMerchant)
  })

  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(messageToUser),
  });

     // RECOMMANDED WAY BUT DOESN T WORK
    // const messages = [
    //     {
    //         to: data.user.expoToken,
    //         sound: 'default',
    //         title: `Prise en charge de votre commande n°${data._id}`,
    //         body: `${data.deliverer.firstName} va s'occuper de vous livrer la commande n°${data._id} ! Vous pouvez désormais discuter ensemble dans la messagerie personnelle`,
    //     },
    //     {
    //         to: data.merchant.expoToken,
    //         sound: 'default',
    //         title: `Prise en charge de la commande n°${data._id}`,
    //         body: `${data.deliverer.firstName} va venir récupérer la commande n°${data._id}, veuillez la préparer s'il vous plait`,
    //     }
    // ]
    // await fetch('https://exp.host/--/api/v2/push/send', {
    //       method: 'POST',
    //       headers: {
    //         'host': 'exp.host',
    //         Accept: 'application/json',
    //         'Accept-encoding': 'gzip, deflate',
    //         'Content-Type': 'application/json',
    //       },
    //       body: JSON.stringify(messages)
    // }).then(response =>{
    //   console.log(response)
    // })

} // End of function

// Notify client when order is in delivery
async function sendNotificationToClientForDelivery(data) {

  const message = {
    to: data.user.expoToken,
    sound: 'default',
    title: `Commande n°${data._id}`,
    body: `${data.deliverer.firstName} a récupéré votre commande \n Veuillez rester disponible s'il vous plait !`,
  };
    
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      'host': 'exp.host',
      'Accept': 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });

} // End of function

// Notify client for new message
async function sendNotificationToClientForNewMessage(data){
  const message = {
    to: data.user.expoToken,
    sound: 'default',
    title: `Nouveau message `,
    body: `${data.deliverer.firstName} vous a envoyé un nouveau message`,
  };
    
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      'host': 'exp.host',
      'Accept': 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });
} // End of function

// Notify deliverer for new message
async function sendNotificationToDelivererForNewMessage(data){
  const message = {
    to: data.deliverer.expoToken,
    sound: 'default',
    title: `Nouveau message `,
    body: `${data.user.firstName} vous a envoyé un nouveau message`,
  };
    
  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      'host': 'exp.host',
      'Accept': 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });
} // End of function

// RUN SERVER
http.listen(PORT, () => {
    console.log(`Notifications server has started at : http://localhost:${PORT}`);
});